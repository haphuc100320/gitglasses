import React, { Component } from 'react'
import Model from './Model'

export default class Glasses extends Component {
    state= {
        imgGlasses : "./BTGLASSES/glassesImage/v1.png"
        
    }
    renderHandLe=(color)=>{
        this.setState({imgGlasses : `./BTGLASSES/glassesImage/${color}.png`})
    }







    render() {
        let imgSrc="./BTGLASSES/glassesImage/model.jpg"
        return (
            <div className='container py-5' >
               <div className="col-4">
                <Model/>
                <img className='img-glasses w-50' src={this.state.imgGlasses} alt="" />
               </div>
                <div className="col-7">
                  <div className="button-img col-12 d-flex">
                    <button onClick={()=>{
                        this.renderHandLe("v1")
                    }} className="img-jpg">
                        <img className='w-25' src="./BTGLASSES/glassesImage/g1.jpg" alt="" />
                    </button>
                    <button onClick={()=>{
                        this.renderHandLe("v2")
                    }} className="img-jpg">
                        <img className='w-25' src="./BTGLASSES/glassesImage/g2.jpg" alt="" />
                    </button>
                    <button onClick={()=>{
                        this.renderHandLe("v3")
                    }} className="img-jpg">
                        <img className='w-25' src="./BTGLASSES/glassesImage/g3.jpg" alt="" />
                    </button>
                    <button onClick={()=>{
                        this.renderHandLe("v4")
                    }} className="img-jpg">
                        <img className='w-25' src="./BTGLASSES/glassesImage/g4.jpg" alt="" />
                    </button>
                    <button onClick={()=>{
                        this.renderHandLe("v5")
                    }} className="img-jpg">
                        <img className='w-25' src="./BTGLASSES/glassesImage/g5.jpg" alt="" />
                    </button>
                    <button onClick={()=>{
                        this.renderHandLe("v6")
                    }} className="img-jpg">
                        <img className='w-25' src="./BTGLASSES/glassesImage/g6.jpg" alt="" />
                    </button>
                    <button onClick={()=>{
                        this.renderHandLe("v7")
                    }} className="img-jpg">
                        <img className='w-25' src="./BTGLASSES/glassesImage/g7.jpg" alt="" />
                    </button>
                    <button onClick={()=>{
                        this.renderHandLe("v8")
                    }} className="img-jpg">
                        <img className='w-25' src="./BTGLASSES/glassesImage/g8.jpg" alt="" />
                    </button>
                    <button onClick={()=>{
                        this.renderHandLe("v9")
                    }} className="img-jpg">
                        <img className='w-25' src="./BTGLASSES/glassesImage/g9.jpg" alt="" />
                    </button>
                    
                  </div>
                    
                    
                </div>
            </div>
        )
    }
}
